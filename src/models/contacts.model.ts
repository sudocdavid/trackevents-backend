import mongoose from 'mongoose';

export interface IContacts extends mongoose.Document {
    userId: mongoose.Types.ObjectId;
    contacts: mongoose.Types.ObjectId[];
};

export const ContactsSchema = new mongoose.Schema({
    userId: mongoose.Types.ObjectId,
    contacts: [mongoose.Types.ObjectId], // user id's
}, { collection: 'contacts', versionKey: false });

const Contacts = mongoose.model<IContacts>('Contacts', ContactsSchema);
export default Contacts;