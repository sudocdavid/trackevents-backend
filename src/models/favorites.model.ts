import mongoose from 'mongoose';

export interface IFavorites extends mongoose.Document {
    userId: mongoose.Types.ObjectId;
    favorites: string[];
};

export const FavoritesSchema = new mongoose.Schema({
    userId: { type: mongoose.Types.ObjectId, required: true },
    favorites: [String], // event id's
}, { collection: 'favorites', versionKey: false });

const Favorites = mongoose.model<IFavorites>('Favorites', FavoritesSchema);
export default Favorites;