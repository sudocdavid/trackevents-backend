import mongoose from 'mongoose';

export interface IUser extends mongoose.Document {
    name: string;
    password: string;
    country: string;
};

export const UserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    password: { type: String, required: true},
    country: String,
}, { collection: 'users', versionKey: false });

const User = mongoose.model<IUser>('User', UserSchema);
export default User;