import mongoose from 'mongoose';
const Schema = mongoose.Schema;

export interface ISharedEvents extends mongoose.Document {
    senderId: mongoose.Types.ObjectId;
    receiverId: mongoose.Types.ObjectId;
    eventId: string;
};

const SharedEventsSchema: mongoose.Schema = new Schema({
    senderId: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    receiverId: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    eventId: {
        type: String,
        required: true
    }
}, { collection: 'sharedEvents', versionKey: false });

const SharedEvents = mongoose.model<ISharedEvents>('SharedEvents', SharedEventsSchema);
export default SharedEvents;