import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import { json } from 'body-parser';
import User, { IUser } from './models/user.model';
import SharedEvents, { ISharedEvents } from './models/sharedEvents.model';
import Favorites, { IFavorites } from './models/favorites.model';
import { IEvent } from './models/event.model';
import Contacts, { IContacts } from './models/contacts.model';

const app = express();
const usersRoutes = express.Router(), favoritesRoutes = express.Router(), contactsRoutes = express.Router(), sharedEventsRoutes = express.Router();
const PORT = 4000;

// let express use cors and json
app.use(cors());
app.use(json());

// Connect to Atlas MongDB
mongoose.connect('mongodb+srv://user_admin:Hallo1234@clusteruni.qgnr0.mongodb.net/trackevents?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true });
const connection = mongoose.connection;
connection.once('open', function () {
    console.log("MongoDB database connection established successfully");
});

// default response
app.get('/', (req: any, res: any) => res.send('Welcome to TrackEvents MongoDB Server!'));
app.use('/users', usersRoutes);
app.use('/favorites', favoritesRoutes); // auth 
app.use('/contacts', contactsRoutes); // auth
app.use('/sharedEvents', sharedEventsRoutes); //auth
app.use("*", (req, res) => {
    res.send("<h1>Welcome to our trackevents mongodb server</h1>");
});

/* users routing */
// returns whole list of users
// usage: [GET] - <IP>:<PORT>/users
usersRoutes.route('/').get((req: any, res: any) => {
    // might remove later
    User.find((err, users: typeof User[]) => {
        if (err) console.log(err);
        else res.json(users);
    });
});

// returns user if the credentials are correct
// usage: [GET] - <IP>:<PORT>/users/login
usersRoutes.route('/login').post((req: any, res: any) => {
    const user: IUser = new User(req.body);

    User.findOne({ name: user.name, password: user.password }, (err: any, doc: IUser | null) => {
        if (err) res.status(400).json({ "Info": "Couldn't find users" });
        else if (doc == null) {
            res.status(401).json({ "Info": "Unauthorized: Wrong user credentials." });
        } else {
            res.status(200).json({ "Info": "Success, correct credentials", "_id": doc._id });
        }
    });
});

// req.params.id must contain user id (inside request url)
// usage: [GET] - <IP>:<PORT>/users/<_idToGet>
usersRoutes.route('/get/:id').get((req: any, res: any) => {
    const id: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    User.findById(id, (err: mongoose.CallbackError, user: IUser) => res.status(200).json(user)).catch(err => console.log(err));
});

// returns userObjectId for given user name in req.body.name
// usage: [GET] - <IP>:<PORT>/users/findUserId
usersRoutes.route('/findUserId').get((req: any, res: any) => {
    const name: string = req.query.name;
    User.findOne({ name: name }, (err: any, user: IUser) => {
        if (user == null) return res.status(404).send("user not found");
        res.status(200).json({ _id: user._id });
    }).catch((err: any) => res.status(400).send(err));
});

// req.body must contain json object of user
// usage: [POST] - <IP>:<PORT>/users/add - Body: JSON of IUser format
usersRoutes.route('/add').post((req: any, res: any) => {
    const user: IUser = new User(req.body);
    User.exists({ name: user.name }).then(doesExist => {
        if (!doesExist) {
            user.save()
                .then((user: IUser) => res.status(200).json({ 'Info': 'User added successfully.', 'User': user }))
                .catch((err: any) => res.status(400).send(err));
        } else {
            res.status(403).send('user already exists.');
        }
    }).catch((err: any) => res.status(400).send(err));

});

// change password (req.body.password) for given user with id in req.params.id
// usage: [GET] - <IP>:<PORT>/users/changeP/<user_id>
usersRoutes.route('/changeP/:id').post((req: any, res: any) => {
    const userId: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    const userPassword: string = req.body.password;
    User.updateOne({ _id: userId }, { $set: { "password": userPassword } }, null, (err: any, user: IUser) => {
        if (err) console.log(err);
        else res.json(user);
        console.log('Password updated' + user);
    })
});

// change country (req.body.country) for given user with id in req.param.id
// usage: [GET] - <IP>:<PORT>/users/changeC/<user_id>
usersRoutes.route('/changeC/:id').post((req: any, res: any) => {
    const userId: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    const userCountry: string = req.body.country;
    User.updateOne({ _id: userId }, { $set: { "country": userCountry } }, null, (err: any, user: IUser) => {
        if (err) console.log(err);
        else res.json(user);
    })
});

// req.params.id must contain user id (inside request url)
// usage: [POST] - <IP>:<PORT>/users/remove/<_idToRemove>
usersRoutes.route('/remove/:id').post((req: any, res: any) => {
    let user: IUser | null;
    User.findByIdAndDelete(req.params.id, null, (err: any, doc: IUser | null) => {
        if (err) console.log(err);
        else console.log("Removed User: ", user = doc);
    }).then(() => res.status(200).json({ 'Info': 'User removed successfully.', 'User': user }))
        .catch((err: any) => res.status(400).send(err))
});


/* favorites routing */
// req.params.id must contain user id (inside request url)
// usage: [POST] - <IP>:<PORT>/favorites/<user_id>
favoritesRoutes.route('/:id').get((req: any, res: any) => {
    const id: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    initFavoritesIfAbsent(id).then(() => {
        Favorites.findOne({ userId: id }, (err: any, favs: IFavorites) => {
            if (err) console.log(err);
            else res.json(favs)
        }).catch((err: any) => res.status(400).send(err));
    });
});

function initFavoritesIfAbsent(userId: mongoose.Types.ObjectId): Promise<boolean> {
    return Favorites.exists({ userId: userId }).then(async doFavoritesExist => {
        if (!doFavoritesExist) {
            const favorites: IFavorites = new Favorites({ userId: userId, favorites: [] });
            await favorites.save()
                .then((favorites: IFavorites) => console.log("Added favorites for" + userId))
                .catch((err: any) => console.log(err));
        } //else console.log('Contacts do already exist for user with id ' + userId + '. No initialization necessary.');
        return !doFavoritesExist;
    });
}

// init an empty favoriteslist for the user
// usage: [POST] - <IP>:<PORT>/favorites/init/<user_id>
favoritesRoutes.route('/init/:id').post((req: any, res: any) => {
    initFavoritesIfAbsent(mongoose.Types.ObjectId(req.params.id)).then(() => res.status(200));
});


// add event to favoriteslist
// usage: [POST] - <IP>:<PORT>/favorites/add/<user_id>
favoritesRoutes.route('/add/:id').post((req: any, res: any) => {
    const userId: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    initFavoritesIfAbsent(userId).then(() => {
        const favEventId: string = req.body.favEventId;
        console.log(favEventId)
        Favorites.updateOne({ userId: userId }, { $addToSet: { favorites: favEventId } }, null, (err: any, favs: IFavorites) => {
            if (err) console.log(err);
            else res.json(favs);
        });
    })
});

function initContactsIfAbsent(userId: mongoose.Types.ObjectId): Promise<boolean> {
    return Contacts.exists({ userId: userId }).then(async doContactsExist => {
        if (!doContactsExist) {
            const contacts: IContacts = new Contacts({ userId: userId, contacts: [] });
            await contacts.save()
                .then((contacts: IContacts) => console.log("Added contacts for" + userId))
                .catch((err: any) => console.log(err));
        } //else console.log('Contacts do already exist for user with id ' + userId + '. No initialization necessary.');
        return !doContactsExist;
    });
}

/* contacts routing */
// req.params.id must contain user id (inside request url)
// usage: [POST] - <IP>:<PORT>/contacts/<user_id>
contactsRoutes.route('/:id').get((req, res) => {
    const id: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    initContactsIfAbsent(id).then(() => {
        Contacts.findOne({ userId: id }, (err: any, contacts: IContacts) => {
            if (err) console.log(err);
            else res.json(contacts)
        }).catch((err: any) => res.status(400).send(err));
    });

});

// init an empty contactslist for the user
// usage: [POST] - <IP>:<PORT>/contacts/init/<user_id>
contactsRoutes.route('/init/:id').post((req, res) => {
    initContactsIfAbsent(mongoose.Types.ObjectId(req.params.id)).then(() => res.status(200));
});

// add contact (different userId) to contactslist
// usage: [POST] - <IP>:<PORT>/contacts<user_id>
contactsRoutes.route('/add/:id').post((req, res) => {
    const userId: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    initContactsIfAbsent(userId).then(() => {
        const contactId: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.body.contactId);
        Contacts.updateOne({ userId: userId }, { $addToSet: { contacts: contactId } }, null, (err: any, contacts: IContacts) => {
            if (err) console.log(err);
            else res.json(contacts);
        });
    });
});

// remove contact (different userId) to contactslist
// usage: [POST] - <IP>:<PORT>/contacts/remove/<user_id> with removeable user id in body
contactsRoutes.route('/remove/:id').post((req, res) => {
    const userId: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    initContactsIfAbsent(userId).then(() => {
        const contactId: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.body.contactId);
        Contacts.updateOne({ userId: userId }, { $pull: { contacts: contactId } }, null, (err: any, contacts: IContacts) => {
            if (err) console.log(err);
            else res.json(contacts);
        });
    });
});

/* sharedEvents routing */
// req.params.id must contain user id (inside request url)
// usage: [POST] - <IP>:<PORT>/sharedEvents/<user_id>
sharedEventsRoutes.route('/:id').get((req, res) => {
    const id: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    SharedEvents.find({ receiverId: id }, (err: any, sharedEvents: ISharedEvents) => {
        if (err) console.log(err);
        else res.json(sharedEvents)
    }).catch((err: any) => res.status(400).send(err));
});

// add a sharedEvent with the sender and receiver
// usage: [POST] - <IP>:<PORT>/sharedEvents/add/<user_id>
sharedEventsRoutes.route('/add/:id').post((req, res) => {
    const receiverId: mongoose.Types.ObjectId = mongoose.Types.ObjectId(req.params.id);
    const senderId: string = req.body["senderId"] as string;
    const eventID: string = req.body["eventId"] as string;
    const sharedEvents: ISharedEvents = new SharedEvents({ receiverId: receiverId, senderId: senderId, eventId: eventID });
    sharedEvents.save()
        .then((sharedEvents: ISharedEvents) => res.status(200).json({ 'Info': 'SharedEvents (empty) init successfully.', 'UserID': receiverId }))
        .catch((err: any) => res.status(400).send(err));
});

app.listen(process.env.PORT || PORT, function () {
    console.log("Server is running on Port: " + PORT);
});